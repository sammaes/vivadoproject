#include <errno.h>
#include <fcntl.h> //open
#include <unistd.h> //close
#include <sys/mman.h> //mmap
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "../../globalConfig.h"

#define OCM_LOC 	0xFFFC0000
#define OCM_SIZE 	256*1024
#define OFFSETDATA	STATUSADDR-OCM_LOC // Hoeveel bytes na OCM_LOC staat gevraagde data (STATUSADDR in globalConfig.h)
#define SW_INT_ID	0

int main(int argc, char* argv[]) {
	int 					err = 0;
	off_t					dev_base = OCM_LOC;
	size_t					ldev = OCM_SIZE;
	unsigned long			mask = OCM_SIZE-1;
	unsigned long			value;

	void					*mapped_base;
	unsigned long volatile	result = 0;
	int						memfd;

	memfd = open("/dev/mem", O_RDWR | O_SYNC);
	if(memfd < 0) {
		err = errno;
		printf("open %d\n",err);
		goto err_;
	}

	mapped_base = mmap(0, ldev, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, dev_base & ~mask);
	if(mapped_base == MAP_FAILED) {
			err = errno;
			printf("mmap %d",err);
			goto err_close;
	}

	//mapped_dev_base = mapped_base + (dev_base & mask);
	if (argv[1][0] == 'r')
	{
		while(1)
		{
			if (*((volatile unsigned long *) (mapped_base + OFFSETDATA)) != result)
			{
				result = *((volatile unsigned long *) (mapped_base + OFFSETDATA));
				printf("Read:\t New Value=0x%lx=%lu\n",result,result);
			}
		}
	}
	else if (argv[1][0] == 'w') // write
	{
		value = strtoul (argv[2], NULL, 0);
		*((volatile unsigned long *) (mapped_base + OFFSETDATA)) = value;
		printf("Write:\t@%p=0x%lx=%ld\n",((volatile unsigned long *) (mapped_base + OFFSETDATA)),value,value);
	}
	else
	{
		printf("Usage: testFirmware r | (w unsigned long)");
	}

	err_munmap:
		munmap(mapped_base, ldev);
	err_close:
		if(memfd > 0) close(memfd);
	err_:
		return err;
}

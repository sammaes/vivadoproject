/*
 * main.h
 *
 *  Created on: Dec 6, 2015
 *      Author: sam
 */

#ifndef MAIN_H_
#define MAIN_H_

/* Includes */
#include <stdio.h>
#include <string.h>
#include <fcntl.h> //open
#include <unistd.h> //close
#include <sys/mman.h> //mmap
#include <errno.h>

#include "../../freertos_minimum_bsp/ps7_cortexa9_1/include/xtime_l.h"

/* Defines */
	// Argument 1: IPC Method + help
	#define OCM			'o'
	#define DMA			'a'
	#define DDR			'd'
	#define RPMSG		'r'
	#define PLBRAM		'b'
	#define HELP		'h'

	// Argument 2: Case */
	#define PID			'p'
	#define IMG			'i'
	#define LINKEDLIST	'l'

	// OCM
	#define OCM_SIZE 	256*1024
	#define OCM_LOC 	0xFFFC0000

	//Global Timer
	#define	GT_SIZE		getpagesize()
	#define GT_LOC		GLOBAL_TMR_BASEADDR

/* Pointer to IPC function (ocm|dma|ddr|rpmsg|plbram) to minimalise overhead once measuring */
int (*ipcmethodfuncptr)(char,void*);

/* Function prototypes */
	// Initialisation functions (STEP1)
	int		init(int argc, char **argv,char *method);
	void 	help(char *execName);
	int 	parseArguments(char method, char cases, char *execName);

	int mapped_dev_baseInit(int *memfd,void **mapped_dev_base,off_t dev_base,size_t length);
	int ocmInit(int *memfd);

	int globalTimerInit(int *memfd);

	void XTime_GetTimeLinux(XTime *Xtime,void *mapped_dev_base);

	// IPC functions (STEP3)
	int 	ocm(char c,void *mapped_dev_base);
	int 	dma(char c,void *mapped_dev_base);
	int 	ddr(char c,void *mapped_dev_base);
	int 	rpmsg(char c,void *mapped_dev_base);
	int 	plbram(char c,void *mapped_dev_base);

	int cleanup(int *memfd, void **mapped_dev_base,size_t length);

#endif /* MAIN_H_ */

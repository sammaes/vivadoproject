/*
 * Application to test and measure Inter Processor Communication on Zynq SoC.
 * Sam Maes 2015-2016
 */

// Referentie: interrupts afzetten
// asm ("nop") voor het aantal instructies
// interrupts aanzetten

#include "main.h"

int main(int argc, char *argv[])
{
	int		ret;
	char	method;
	int 	memfdOCM = -1;
	int 	memfdGT = -1;
	void 	*mapped_dev_baseOCM = NULL;	// OCM resource
	void 	*mapped_dev_baseGT 	= NULL;	// GT resource
	void	**mapped_dev_base 	= NULL;								//point to correct ipc mapped_dev_base
	XTime 	tStart, tEnd;

	// STEP1: Initialisation
		// Check if correct parameters are given and assigns ipcmethodfuncptr to correct function
		if ( init(argc, argv,&method) == -1)
			return -1;

		// Init IPC
		switch(method)
		{
			case OCM: 	if (mapped_dev_baseInit(&memfdOCM,&mapped_dev_baseOCM,OCM_LOC,OCM_SIZE) != 0) {
							printf("OCM init failed\n");
							return -1;
						}
						mapped_dev_base = &mapped_dev_baseOCM;
						break;
			case DMA: 	/*dmaCleanup();*/ 		break;
			case DDR: 	/*ddrCleanup();*/		break;
			case RPMSG:	/*rpmsgCleanup();*/ 	break;
			case PLBRAM:/*plbramCleanup();*/ 	break;
		}

		// Init global timer
		if (mapped_dev_baseInit(&memfdGT,&mapped_dev_baseGT,GT_LOC,GT_SIZE) != 0)
		{
			printf("global timer init failed");
			return -1;
		}

	// STEP2: Measuring start
		XTime_GetTimeLinux(&tStart,mapped_dev_baseGT);

	// STEP3: Start IPC
		ret = ipcmethodfuncptr(argv[2][0],mapped_dev_base); // Pointer to function for minimal overhead

	// STEP4: Measuring stop
		XTime_GetTimeLinux(&tEnd,mapped_dev_baseGT);

	// STEP4A: Do something with result
		printf("Output took %llu clock cycles. RET=%d\n", 2*(tEnd - tStart), ret);

	// STEP5: Cleanup resources
		switch(method)
		{
			case OCM: 		cleanup(&memfdOCM,&mapped_dev_baseOCM,OCM_SIZE);	break;
			case DMA: 		/*dmaCleanup();*/ 		break;
			case DDR: 		/*ddrCleanup();*/		break;
			case RPMSG:		/*rpmsgCleanup();*/ 	break;
			case PLBRAM:	/*plbramCleanup();*/ 	break;
		}

		// Cleanup global timer
		cleanup(&memfdGT,&mapped_dev_baseGT,GT_SIZE);

    return 0;
}

int mapped_dev_baseInit(int *memfd,void **mapped_dev_base,off_t dev_base,size_t length)
{
	int				err = 0;
	unsigned long	mask = length-1;
	void			*mapped_base;

	*memfd = open("/dev/mem", O_RDWR | O_SYNC);
	if(*memfd < 0) {
		err = errno;
		printf("open %d\n",err);
		return err;
	}

	mapped_base = mmap(0, length, PROT_READ | PROT_WRITE, MAP_SHARED, *memfd, dev_base & ~mask);
	if(mapped_base == MAP_FAILED) {
			err = errno;
			printf("mmap %d",err);
			if(*memfd > 0) close(*memfd);
			return err;
	}

    *mapped_dev_base = mapped_base + (dev_base & mask);

	return err;
}

int cleanup(int *memfd, void **mapped_dev_base,size_t length)
{
	if (*mapped_dev_base != NULL) 	munmap(*mapped_dev_base, length);
	if(*memfd > 0) 					close(*memfd);
	return 0;
}

int ocm(char c,void *mapped_dev_base)
{
	int i;
	int wrong = 0;

	for (i=0;i<60000;i++);
	{
		*((volatile unsigned long *) (mapped_dev_base + i*4)) = i;
	}

	for (i=0;i<60000;i++);
	{
		if ( *((volatile unsigned long *) (mapped_dev_base + i*4)) != i) wrong++;
	}

	return wrong;
}

int dma(char c,void *mapped_dev_base)
{
	printf("DMA not implemented yet\n");
	printf("Case:%c\n",c);
	return 0;
}

int ddr(char c,void *mapped_dev_base)
{
	printf("DDR not implemented yet\n");
	printf("Case:%c\n",c);
	return 0;
}

int rpmsg(char c,void *mapped_dev_base)
{
	printf("RPMsg not implemented yet\n");
	printf("Case:%c\n",c);
	return 0;
}

int plbram(char c,void *mapped_dev_base)
{
	printf("PLBRAM not implemented\n");
	printf("Case:%c\n",c);
	return 0;
}

int	init(int argc, char **argv, char *method)
{
	// Test if enough arguments are given
	if (argc < 3)
	{
		help(argv[0]);
		return -1;
	}

	// Parse arguments to assign ipcmethodfuncptr to respective function and make sure correct parameters are given
	if ( parseArguments(argv[1][0],argv[2][0],argv[0]) == -1)
		return -1;

	*method = argv[1][0];
	return 0;
}

void help(char *execName)
{
	printf("Usage %s method case\n",execName);
	printf("Possible methods:\n");
	printf("OCM:\t\t\t%c\nDMA:\t\t\t%c\nDDR:\t\t\t%c\nRPMsg:\t\t\t%c\nPLBRAM:\t\t\t%c\nPrint this message:\t%c\n\n",OCM,DMA,DDR,RPMSG,PLBRAM,HELP);
	printf("Possible cases:\n");
	printf("PID:\t\t%c\nImage:\t\t%c\nLinked list:\t%c\n",PID,IMG,LINKEDLIST);
}

int parseArguments(char method, char cases, char *execName)
{
	if ( (cases != PID) && (cases != IMG) && (cases != LINKEDLIST) )
	{
		help(execName);
		return -1;
	}

	switch(method)
	{
		case OCM: 	ipcmethodfuncptr = &ocm;
					break;
		case DMA: 	ipcmethodfuncptr = &dma;
					break;
		case DDR: 	ipcmethodfuncptr = &ddr;
					break;
		case RPMSG:	ipcmethodfuncptr = &rpmsg;
					break;
		case PLBRAM:ipcmethodfuncptr = &plbram;
					break;
		case HELP:	//no break here
		default:	help(execName);
					return -1;
	}

	return 0;
}

//Get the time from the Global Timer Counter Register.
void XTime_GetTimeLinux(XTime *Xtime,void *mapped_dev_base)
{
	unsigned long low = 0;
	unsigned long high = 0;

	// Reading Global Timer Counter Register
	do
	{
		high = *((volatile unsigned long *) (mapped_dev_base + GTIMER_COUNTER_UPPER_OFFSET));
		low  = *((volatile unsigned long *) (mapped_dev_base + GTIMER_COUNTER_LOWER_OFFSET));
	} while((*((volatile unsigned long *) (mapped_dev_base + GTIMER_COUNTER_UPPER_OFFSET))) != high);

	*Xtime = (((XTime) high) << 32) | (XTime) low;
}

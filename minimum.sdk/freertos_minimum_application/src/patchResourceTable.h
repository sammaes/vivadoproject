/*
 * patchResourceTable.h
 *
 *  Created on: Dec 1, 2015
 *      Author: sam
 */

#ifndef PATCHRESOURCETABLE_H_
#define PATCHRESOURCETABLE_H_
/*
#define RAM_ADDR 0x00100000 //240Mb

struct resource_table {//Just copied from linux/remoteproc.h
	u32	 ver;//Must be 1 for remoteproc module!
	u32	 num;
 	u32	 reserved[2];
 	u32	 offset[1];
} __packed;

enum fw_resource_type {
	RSC_CARVEOUT 	= 0,
	RSC_DEVMEM 		= 1,
	RSC_TRACE 		= 2,
	RSC_VDEV 		= 3,
	RSC_MMU  		= 4,
	RSC_LAST 		= 5,
};

struct fw_rsc_carveout {
	u32 	type;//from struct fw_rsc_hdr
	u32 	da;
	u32 	pa;
	u32 	len;
	u32 	flags;
	u32 	reserved;
	u8 		name[32];
} __packed;

__attribute__ ((section (".rtable")))
const struct rproc_resource {
    struct resource_table base;
    //u32 offset[4];
    struct fw_rsc_carveout code_cout;
} ti_ipc_remoteproc_ResourceTable = {
 .base = { .ver = 1, .num = 1, .reserved = { 0, 0 },
   .offset = { offsetof(struct rproc_resource, code_cout) },
 },
 .code_cout = {
    .type = RSC_CARVEOUT, .da = RAM_ADDR, .pa = RAM_ADDR, .len = 1<<19,
    .flags=0, .reserved=0, .name="CPU1CODE",
 },
};
*/

extern char *_start; // ELF_START should be zero all the time
#define ELF_START				(unsigned int)&_start
extern char *_end;
#define ELF_END					(unsigned int)&_end
// section helpers
//#define __section(S)			__attribute__((__section__(#S)))
//#define __resource				__section(.resource_table)
#define __section(S)			__attribute__((__section__(#S)))
#define __resource				__section(.resource_table)
#define TYPE_CARVEOUT			0

struct fw_rsc_carveout {
	u32 	type;//from struct fw_rsc_hdr
	u32 	da;
	u32 	pa;
	u32 	len;
	u32 	flags;
	u32 	reserved;
	u8 		name[32];
} __packed;

struct resource_table {//Just copied from linux/remoteproc.h
	u32	 ver;//Must be 1 for remoteproc module!
	u32	 num;
 	u32	 reserved[2];
 	u32	 offset[2];
 	struct fw_rsc_carveout text_cout;
} __packed;

struct resource_table __resource resources = {
	1, // we're the first version that implements this
	1, // number of entries in the table
	{ 0, 0, }, // reserved, must be zero
	// offsets to entries
	{
		offsetof(struct resource_table, text_cout),
	},

	// End of ELF file
	{ TYPE_CARVEOUT, 0, 0, ELF_END, 0, 0, "TEXT/DATA", },
};

#endif /* PATCHRESOURCETABLE_H_ */

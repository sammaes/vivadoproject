/*
 Copyright (C) 2012-2013 Xilinx, Inc.

 This file is part of the port for FreeRTOS made by Xilinx to allow FreeRTOS
 to operate with Xilinx Zynq devices.

 This file is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License (version 2) as published by the
 Free Software Foundation AND MODIFIED BY the FreeRTOS exception
 (see text further below).

 This file is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License; if not it
 can be viewed here: <http://www.gnu.org/licenses/>

 The following exception language was found at
 http://www.freertos.org/a00114.html on May 8, 2012.

 GNU General Public License Exception

 Any FreeRTOS source code, whether modified or in its original release form,
 or whether in whole or in part, can only be distributed by you under the
 terms of the GNU General Public License plus this exception. An independent
 module is a module which is not derived from or based on FreeRTOS.

 EXCEPTION TEXT:

 Clause 1

 Linking FreeRTOS statically or dynamically with other modules is making a
 combined work based on FreeRTOS. Thus, the terms and conditions of the
 GNU General Public License cover the whole combination.

 As a special exception, the copyright holder of FreeRTOS gives you permission
 to link FreeRTOS with independent modules that communicate with FreeRTOS
 solely through the FreeRTOS API interface, regardless of the license terms
 of these independent modules, and to copy and distribute the resulting
 combined work under terms of your choice, provided that

 Every copy of the combined work is accompanied by a written statement that
 details to the recipient the version of FreeRTOS used and an offer by
 yourself to provide the FreeRTOS source code (including any modifications
 you may have  made) should the recipient request it.
 The combined work is not itself an RTOS, scheduler, kernel or related product.
 The independent modules add significant and primary functionality to FreeRTOS
 and do not merely extend the existing functionality already present
 in FreeRTOS.

 Clause 2

 FreeRTOS may not be used for any competitive or comparative purpose,
 including the publication of any form of run time or compile time metric,
 without the express permission of Real Time Engineers Ltd. (this is the norm
 within the industry and is intended to ensure information accuracy).

*/

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
//#include "xil_printf.h"
#include "xuartps.h"
#include "xparameters.h"
#include "xil_io.h"
#include "xil_mmu.h"
#include "xil_exception.h"
#include "xscugic.h"
#include <unistd.h>

#include "../../globalConfig.h"
#include "patchResourceTable.h"

/* Priorities at which the tasks are created. */
#define mainHELLO_WORLD_TASK_PRIORITY		( tskIDLE_PRIORITY + 2 )
#define	mainGOOD_BYE_TASK_PRIORITY			( tskIDLE_PRIORITY + 1 )
#define INTC_DEVICE_ID	XPAR_SCUGIC_0_DEVICE_ID

int ScuGicExample(u16 DeviceId);
int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr);
void DeviceDriverHandler(void *CallbackRef);

uint32_t adres = STATUSADDR;
XScuGic InterruptController; 	     //Instance of the Interrupt Controller
static XScuGic_Config *GicConfig;    //The configuration parameters of the controller

/*-----------------------------------------------------------*/
static void prvHelloWorld( void *pvParameters );
static void prvGoodBye( void *pvParameters );

int main( void )
{
	XScuGic_SoftwareIntr(&InterruptController, 0, XSCUGIC_SPI_CPU0_MASK);
	prvInitializeExceptions();

	/* Start the two tasks */
	xTaskCreate( prvHelloWorld, ( signed char * ) "HW",configMINIMAL_STACK_SIZE, NULL,mainHELLO_WORLD_TASK_PRIORITY, NULL );
	xTaskCreate( prvGoodBye, ( signed char * ) "GB",configMINIMAL_STACK_SIZE, NULL,mainGOOD_BYE_TASK_PRIORITY, NULL );

	/* Start the tasks and timer running. */
	Xil_SetTlbAttributes(0xFFFC0000,0x14de2); // S=b1 TEX=b100 AP=b11, Domain=b1111, C=b0, B=b0
	vTaskStartScheduler();

	/* If all is well, the scheduler will now be running, and the following line
	will never be reached.  If the following line does execute, then there was
	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
	to be created.  See the memory management section on the FreeRTOS web site
	for more details. */
	uint32_t output = 1111111111;
	for( ;; )
		Xil_Out32(adres,output);
}


/*-----------------------------------------------------------*/
static void prvHelloWorld( void *pvParameters )
{
	uint32_t output = 1111122222;

	// Perform an action every 10 ticks.
	portTickType xLastWakeTime;
	const portTickType xFrequency = 10 / portTICK_RATE_MS;


	// Initialise the xLastWakeTime variable with the current time.
	xLastWakeTime = xTaskGetTickCount();

	for( ;; )
	{
		// Wait for the next cycle.
		vTaskDelayUntil( &xLastWakeTime, xFrequency );

		// Perform action here.
		//xil_printf("Hello World\r\n");
		Xil_Out32(adres,output);
	}
}

/*-----------------------------------------------------------*/
static void prvGoodBye( void *pvParameters )
{
	uint32_t output = 1111133333;
	for( ;; )
	{
		//xil_printf("Good Bye\r\n");
		Xil_Out32(adres,output);
		usleep(500000); //wait 500ms
	}
}


/*-----------------------------------------------------------*/
void vApplicationMallocFailedHook( void )
{
	/* vApplicationMallocFailedHook() will only be called if
	configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
	function that will get called if a call to pvPortMalloc() fails.
	pvPortMalloc() is called internally by the kernel whenever a task, queue or
	semaphore is created.  It is also called by various parts of the demo
	application.  If heap_1.c or heap_2.c are used, then the size of the heap
	available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
	to query the size of free heap space that remains (although it does not
	provide information on how the remaining heap might be fragmented). */
	taskDISABLE_INTERRUPTS();
	uint32_t output = 1111144444;
	for( ;; )
		Xil_Out32(adres,output);
}

/*-----------------------------------------------------------*/
void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* vApplicationStackOverflowHook() will only be called if
	configCHECK_FOR_STACK_OVERFLOW is set to either 1 or 2.  The handle and name
	of the offending task will be passed into the hook function via its
	parameters.  However, when a stack has overflowed, it is possible that the
	parameters will have been corrupted, in which case the pxCurrentTCB variable
	can be inspected directly. */
	taskDISABLE_INTERRUPTS();
	uint32_t output = 1111155555;
	for( ;; )
			Xil_Out32(adres,output);
}

void vApplicationSetupHardware( void )
{
	/* Do nothing */
}

int ScuGicExample(u16 DeviceId)
{
	int Status;

	//Initialize the interrupt controller driver so that it is ready to use.
	GicConfig = XScuGic_LookupConfig(DeviceId);
	if (NULL == GicConfig) return XST_FAILURE;

	Status = XScuGic_CfgInitialize(&InterruptController, GicConfig,GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) return XST_FAILURE;


	//Perform a self-test to ensure that the hardware was built correctly
	Status = XScuGic_SelfTest(&InterruptController);
	if (Status != XST_SUCCESS) return XST_FAILURE;

	//Setup the Interrupt System
	Status = SetUpInterruptSystem(&InterruptController);
	if (Status != XST_SUCCESS) return XST_FAILURE;


	/* Connect a device driver handler that will be called when an
	 * interrupt for the device occurs, the device driver handler performs
	 * the specific interrupt processing for the device */
	Status = XScuGic_Connect(&InterruptController, INTC_DEVICE_INT_ID,(Xil_ExceptionHandler)DeviceDriverHandler,(void *)&InterruptController);

	if (Status != XST_SUCCESS) return XST_FAILURE;

	// Enable the interrupt for the device and then cause (simulate) an interrupt so the handlers will be called
	XScuGic_Enable(&InterruptController, INTC_DEVICE_INT_ID);

	//Simulate the Interrupt
	Status = XScuGic_SoftwareIntr(&InterruptController,INTC_DEVICE_INT_ID,XSCUGIC_SPI_CPU0_MASK);
}

int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr)
{

	// Connect the interrupt controller interrupt handler to the hardware interrupt handling logic in the ARM processor.
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,(Xil_ExceptionHandler) XScuGic_InterruptHandler,XScuGicInstancePtr);

	//Enable interrupts in the ARM
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}
